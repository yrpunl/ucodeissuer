require "ucodeIssuer/version"
require 'yaml'
require 'yaml/store'
require 'pp'

module UcodeIssuer
  # Your code goes here...
  class UcodeIssuer
    @@yaml_file = 'ucodeIssuer.yaml'
    @@yaml_store = YAML::Store.new(@@yaml_file)

    def init
      @@yaml_store = YAML::Store.new(@@yaml_file)
    end

    def self.format(f,t)
      YAML.dump({
        'from_ucode' => f,
        'to_ucode'   => t,
        'now_ucode'  => f
      },open(@@yaml_file,'w'))
    end
    
    def self.setReagion(f,t)
      @@yaml = YAML.load_file(@@yaml_file)
      initial = {
        'from_ucode' => f,
        'to_ucode'   => t
      }
      initial['now_ucode']  = f if @@yaml['now_ucode'].nil?
      
      store = YAML::Store.new(@@yaml_file)
      ## データの読み出しと格納
      store.transaction do |hash|
        @@yaml.each do |k,v|
          hash[k] = v
        end
      end
    end
    
    def self.getCurrent
      @@yaml_store.transaction do |hash|
        return hash['now_ucode']
      end
    end
    
    def self.issue
      issueUcode = nil
      @@yaml_store.transaction do |hash|
        issueUcode = hash['now_ucode']
        hash['now_ucode'] = sprintf("%#034X",(hash['now_ucode'].hex + 1)).gsub(/^0X/,"")
      end
      return issueUcode
    end
    
  end
end

# sp @@yaml.class.name
# from = f.hex
# to = t.hex
# sprintf("%#034X",raw_ucode).gsub(/^0X/,"")      
